package main.java.org.glycoinfo.Glycans.Util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;

public class DataFileReader {

	private LinkedList<String> m_strOutput;


	public DataFileReader(final String a_objFilepath) {
		LinkedList<String> a_str = new LinkedList<String>();


	       try {
	            File file = new File(a_objFilepath);

	            if (!file.exists()) {
	                System.out.print("ファイルが存在しません");
	                return;
	            }

	            FileReader fileReader = new FileReader(file);
	            BufferedReader bufferedReader = new BufferedReader(fileReader);
	            String data;
	            while ((data = bufferedReader.readLine()) != null) {
	                System.out.println(data);
	                a_str.add(data);
	            }

	            bufferedReader.close();

	        } catch (IOException e) {
	            e.printStackTrace();
	        }

		/*
		try{
			  File file = new File(a_objFilepath);
			  BufferedReader br = new BufferedReader(new FileReader(file));
			  String str;
			  while((str = br.readLine()) != null){
				if (str.length() > 0) {
					System.out.println(str);
			    	a_str.add(str);
				}
			  }

			  br.close();
		}catch(FileNotFoundException e){
			  System.out.println(e);
		}catch(IOException e){
			  System.out.println(e);
		}
		*/
		this.m_strOutput = a_str;
	}

	public LinkedList<String> getFileStrings() {
		return this.m_strOutput;
	}

}

