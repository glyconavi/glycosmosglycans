package main.java.org.glycoinfo.Glycans.Util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.LinkedList;

public class util {


	public static String getUrlString(String a_Str) {
		String str_out = "";
		str_out = a_Str.replaceAll(",", "_");
		str_out = a_Str.replaceAll(" ", "_");
		str_out = str_out.replaceAll(":", "_");
		str_out = str_out.replaceAll("[*]", "star");
		str_out = str_out.replaceAll("\n", "_");
		str_out = str_out.replaceAll("\t", "_");
		str_out = str_out.replaceAll("__", "_");
		str_out = str_out.replaceAll(",_", "_");
		return str_out;
	}


	public static String getSHA1string(String value) {
        String s_Hashedvalue = "";

        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-1");
            byte[] result = digest.digest(value.getBytes());
            s_Hashedvalue = String.format("%040x", new BigInteger(1, result));
        } catch (Exception e){
            e.printStackTrace();
        }
        //System.out.println( sha1 );
        return s_Hashedvalue;
    }

	public static String replaceCharacter(String a_Str) {
		//String str_out = "";
		String str_out1 = a_Str.replaceAll("℃", " degree Celsius");
		String str_out2 = str_out1.replaceAll("[℃]", " degree Celsius");
		String str_out3 = str_out2.replaceAll("@", "α");
		String str_out4 = str_out3.replaceAll("[@]", "α");
		String str_out = str_out4.replaceAll("[$]", "β"); // $

		return str_out;
	}



	public static String removeTabReturn(String a_Str) {
		String str_out = "";
		  String strline0 = a_Str.replaceAll("\\t", "");
		  String strline1 = strline0.replaceAll("\\r\\n", "");
		  str_out = strline1.replaceAll("\\n", "");
		return str_out;
	}



	public static LinkedList<String[]> separateTab(String a_Str){
		LinkedList<String[]> a_data = new LinkedList<String[]>();
		// separate to lines
		String[] lines = a_Str.split("\n");
		// check tab, tab"{data}"tab
		for (int i=0; i<lines.length; ++i) {
			String t_Str1 = lines[i].replaceAll("\"\t", "\t");
			String t_Str2 = t_Str1.replaceAll("\t\"", "\t");

			String[] data = t_Str2.split("\t");
			if (data.length > 0) {
				a_data.add(data);
			}
		}
		return a_data;
	}



}
