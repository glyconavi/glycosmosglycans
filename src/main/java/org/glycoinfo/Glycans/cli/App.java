package main.java.org.glycoinfo.Glycans.cli;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.DCTerms;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.glycoinfo.GlycanFormatconverter.io.GlycoCT.WURCSToGlycoCT;
import org.glycoinfo.GlycanFormatconverter.io.IUPAC.IUPACStyleDescriptor;
import org.glycoinfo.GlycanFormatconverter.io.WURCS.WURCSImporter;
import org.glycoinfo.GlycanFormatconverter.util.ExporterEntrance;
import org.glycoinfo.WURCSFramework.util.validation.WURCSValidator;

import main.java.org.glycoinfo.Glycans.RDF.ClassUri;
import main.java.org.glycoinfo.Glycans.RDF.PredicateTerms;
import main.java.org.glycoinfo.Glycans.RDF.PrefixTerm;
import main.java.org.glycoinfo.Glycans.Util.DataFileReader;

public class App {

	static final String APP_NAME = "GlyCosmosGlycansSeqRDF";
	static final String APP_VERSION = "0.4.0";

	public static void main(String[] args) throws Exception {
		System.out.println(APP_NAME);
		System.out.println(APP_VERSION);
		new App().run(args);
		System.out.println("Fin.");
	}

	void run(String[] args) throws Exception {

		String t_strFilePath = "";

		for (int i = 0; i < args.length; ++i) {
			if ("-f".equals(args[i])) {
				t_strFilePath = args[++i];
				//System.out.println(t_strFilePath);
			}
		}

		Date dt = new Date();
		SimpleDateFormat dtf = new SimpleDateFormat("_yyyy-MM-dd");
		String dtStr = dtf.format(dt);
		SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
		String dateStr = date.format(dt);

		String GlyCosmosGlycanPath = t_strFilePath + dtStr + "_Glycans-" + APP_VERSION + ".ttl";
		FileWriter writeGSfile = new FileWriter(GlyCosmosGlycanPath, true);
		PrintWriter pwGG = new PrintWriter(new BufferedWriter(writeGSfile));

		File file = new File(t_strFilePath);

		if (file.exists()) {
			String t_filePath = file.toPath().toString();
			//System.out.println(t_filePath);
			DataFileReader reader = new DataFileReader(t_filePath);
			LinkedList<String> lines = reader.getFileStrings();

			String str_GTC = "";
			String str_WURCS = "";
			for (String line : lines) {

				//System.out.println(line);
				//String BASE = "http://glycosmos.org/glycans/";
				// https://identifiers.org/glytoucan:G00054MO
				String BASE = "http://identifiers.org/glytoucan/";

				Model o_model = ModelFactory.createDefaultModel();
				o_model.setNsPrefix("", BASE);

				// line
				// GTC \t WURCS
				String tab_string = line.replaceAll("\"", "");
				String[] a_str = tab_string.split("\t");

				if (a_str.length == 2) {
					str_GTC = a_str[0].trim();
					str_WURCS = a_str[1].trim();
				}
				if (str_GTC.substring(0, 1).equals("G") && str_WURCS.substring(0, 6).equals("WURCS=")) {

					//String Short = "";
					String Condensed = "";
					String Extended = "";
					String GlycoCT = "";
					String Glycam = "";

					try {

						WURCSValidator val = new WURCSValidator();
						val.start(str_WURCS);
						boolean bl = val.getReport().hasError();
						if (bl != true) {
							str_WURCS = val.getReport().getStandardString();
							//} else {
							//	str_WURCS = "";
							//}

							try {
								WURCSImporter wi = new WURCSImporter();
								ExporterEntrance ee;
								ee = new ExporterEntrance(wi.start(str_WURCS));
								//Short = ee.toIUPAC(IUPACStyleDescriptor.SHORT);
								try {
									Condensed = ee.toIUPAC(IUPACStyleDescriptor.CONDENSED);
								} catch (Exception e) {
									System.err.println("[error]\t" + line);
									e.printStackTrace();
								}
								try {
									Extended = ee.toIUPAC(IUPACStyleDescriptor.EXTENDED);
								} catch (Exception e) {
									System.err.println("[error]\t" + line);
									e.printStackTrace();
								}
								try {
									Glycam = ee.toIUPAC(IUPACStyleDescriptor.GLYCANWEB);
								} catch (Exception e) {
									System.err.println("[error]\t" + line);
									e.printStackTrace();
								}
							} catch (Exception e) {
								System.err.println("[error]\t" + line);
								e.printStackTrace();
							}

							// add Prefix
							//o_model.setNsPrefix(PrefixTerm.SIO.getPrefix(), PrefixTerm.SIO.getUri());
							o_model.setNsPrefix(PrefixTerm.SKOS.getPrefix(), PrefixTerm.SKOS.getUri());
							o_model.setNsPrefix(PrefixTerm.RDFS.getPrefix(), PrefixTerm.RDFS.getUri());
							o_model.setNsPrefix(PrefixTerm.RDF.getPrefix(), PrefixTerm.RDF.getUri());
							o_model.setNsPrefix(PrefixTerm.DCTERMS.getPrefix(), PrefixTerm.DCTERMS.getUri());
							//o_model.setNsPrefix(PrefixTerm.GG.getPrefix(), PrefixTerm.GG.getUri());
							o_model.setNsPrefix(PrefixTerm.GLYCAN.getPrefix(), PrefixTerm.GLYCAN.getUri());
							o_model.setNsPrefix(PrefixTerm.COSMOS.getPrefix(), PrefixTerm.COSMOS.getUri());
							o_model.setNsPrefix(PrefixTerm.GTCOWL.getPrefix(), PrefixTerm.GTCOWL.getUri());
							o_model.setNsPrefix(PrefixTerm.INFO.getPrefix(), PrefixTerm.INFO.getUri());

							//Model o_model = ModelFactory.createDefaultModel();
							String EntityResource = PrefixTerm.INFO.getUri() + str_GTC;
							Resource cosmos_glycan = o_model.createResource(EntityResource);
							Resource t_Saccharide_type = ClassUri.Saccharide.getClassUri();
							//Resource r_saccharide_type = o_model.createResource(t_Saccharide_type);
							cosmos_glycan.addProperty(RDF.type, t_Saccharide_type);
							cosmos_glycan.addProperty(DCTerms.identifier, str_GTC);

							// add licence
							Resource ccby_license = o_model
									.createResource("http://creativecommons.org/licenses/by-sa/4.0/");
							Resource license_type = o_model.createResource("http://purl.org/dc/terms/LicenseDocument");
							cosmos_glycan.addProperty(DCTerms.license, ccby_license);
							ccby_license.addProperty(RDF.type, license_type);
							cosmos_glycan.addProperty(DCTerms.hasVersion, dateStr);

							cosmos_glycan.addProperty(o_model.createProperty(
									PredicateTerms.has_primary_id.getPredicate()), str_GTC);
							cosmos_glycan.addProperty(RDFS.label, "GlyTouCan:" + str_GTC);
							//cosmos_glycan.addProperty(o_model.createProperty(PredicateTerms.altLabel.getPredicate()), str_GTC);

							// WURCS
							//String encoded_WURCS = URLEncoder.encode(str_WURCS, "UTF-8");
							String WURCSResource = PrefixTerm.INFO.getUri() + str_GTC + "_wurcs";
							Resource glycoseq_wurcs = o_model.createResource(WURCSResource);
							cosmos_glycan.addProperty(o_model.createProperty(
									PredicateTerms.has_glycosequence.getPredicate()), glycoseq_wurcs);
							Resource t_GlycoSeq_type = ClassUri.GlycoSequence.getClassUri();
							//Resource r_glycoseq = o_model.createResource(t_GlycoSeq_type);
							glycoseq_wurcs.addProperty(RDF.type, t_GlycoSeq_type);
							glycoseq_wurcs.addProperty(o_model.createProperty(
									PredicateTerms.in_carbohydrate_format.getPredicate()),
									ClassUri.carbohydrate_format_wurcs.getClassUri());
							glycoseq_wurcs.addProperty(o_model.createProperty(
									PredicateTerms.has_sequence.getPredicate()), str_WURCS);

							try {
								WURCSToGlycoCT converter = new WURCSToGlycoCT();
								converter.start(str_WURCS);
								if (!converter.getErrorMessages().isEmpty()) {
									System.out.println(converter.getErrorMessages());
								}
								GlycoCT = converter.getGlycoCT();
							} catch (Exception e) {
								System.err.println("[error]\t" + line);
								e.printStackTrace();
							}
							// IUPAC Extended
							if (Extended != "") {
								//String encoded_Extended = URLEncoder.encode(Extended, "UTF-8");
								String ExtendedResource = PrefixTerm.INFO.getUri() + str_GTC + "_iupac_extended";
								Resource glycoseq_Extended = o_model.createResource(ExtendedResource);
								cosmos_glycan.addProperty(o_model.createProperty(
										PredicateTerms.has_glycosequence.getPredicate()), glycoseq_Extended);
								glycoseq_Extended.addProperty(RDF.type, t_GlycoSeq_type);
								glycoseq_Extended.addProperty(o_model.createProperty(
										PredicateTerms.in_carbohydrate_format.getPredicate()),
										ClassUri.carbohydrate_format_iupac_extended.getClassUri());
								glycoseq_Extended.addProperty(o_model.createProperty(
										PredicateTerms.has_sequence.getPredicate()), Extended);
							}

							// IUPAC condensed
							if (Condensed != "") {
								//String encoded_Condensed = URLEncoder.encode(Condensed, "UTF-8");
								String CondensedResource = PrefixTerm.INFO.getUri() + str_GTC + "_iupac_condensed";
								Resource glycoseq_Condensed = o_model.createResource(CondensedResource);
								cosmos_glycan.addProperty(o_model.createProperty(
										PredicateTerms.has_glycosequence.getPredicate()), glycoseq_Condensed);
								glycoseq_Condensed.addProperty(RDF.type, t_GlycoSeq_type);
								glycoseq_Condensed.addProperty(o_model.createProperty(
										PredicateTerms.in_carbohydrate_format.getPredicate()),
										ClassUri.carbohydrate_format_iupac_condensed.getClassUri());
								glycoseq_Condensed.addProperty(o_model.createProperty(
										PredicateTerms.has_sequence.getPredicate()), Condensed);
								cosmos_glycan.addProperty(
										o_model.createProperty(PredicateTerms.altLabel.getPredicate()), Condensed);
							}

							/*
							// IUPAC short
							if (Short != "") {
								//String encoded_Short = URLEncoder.encode(Short, "UTF-8");
								String ShortResource = PrefixTerm.INFO.getUri() + str_GTC + "_iupac_short";
								Resource glycoseq_Short = o_model.createResource(ShortResource);
								cosmos_glycan.addProperty(o_model.createProperty(
										PredicateTerms.has_glycosequence.getPredicate())
										, glycoseq_Short);
								glycoseq_Short.addProperty(RDF.type, t_GlycoSeq_type);
								glycoseq_Short.addProperty(o_model.createProperty(
										PredicateTerms.in_carbohydrate_format.getPredicate())
										, ClassUri.carbohydrate_format_iupac_short.getClassUri());
								glycoseq_Short.addProperty(o_model.createProperty(
										PredicateTerms.has_sequence.getPredicate()), Short);
							}

							*/

							// GlycoCT
							if (GlycoCT != null && GlycoCT != "") {
								//String encoded_GlycoCT = URLEncoder.encode(GlycoCT, "UTF-8");
								String GlycoCTResource = PrefixTerm.INFO.getUri() + str_GTC + "_glycoct";
								Resource glycoseq_GlycoCT = o_model.createResource(GlycoCTResource);
								cosmos_glycan.addProperty(o_model.createProperty(
										PredicateTerms.has_glycosequence.getPredicate()), glycoseq_GlycoCT);
								glycoseq_GlycoCT.addProperty(RDF.type, t_GlycoSeq_type);
								glycoseq_GlycoCT.addProperty(o_model.createProperty(
										PredicateTerms.in_carbohydrate_format.getPredicate()),
										ClassUri.carbohydrate_format_glycoct.getClassUri());
								glycoseq_GlycoCT.addProperty(o_model.createProperty(
										PredicateTerms.has_sequence.getPredicate()), GlycoCT);
							}

							// GLYCAM Seq
							if (Glycam != "") {
								String GlycamResource = PrefixTerm.INFO.getUri() + str_GTC + "_glycam_condensed";
								Resource glycoseq_Glycam = o_model.createResource(GlycamResource);
								cosmos_glycan.addProperty(o_model.createProperty(
										PredicateTerms.has_glycosequence.getPredicate()), glycoseq_Glycam);
								glycoseq_Glycam.addProperty(RDF.type, t_GlycoSeq_type);
								glycoseq_Glycam.addProperty(o_model.createProperty(
										PredicateTerms.in_carbohydrate_format.getPredicate()),
										ClassUri.carbohydrate_format_glycam_condensed.getClassUri());
								glycoseq_Glycam.addProperty(o_model.createProperty(
										PredicateTerms.has_sequence.getPredicate()), Glycam);

								// http://glycam.org/url?condensed=DManpa1-2DManpa1-3DManpa1-6%5BDManpa1-3%5DDManpb1-4DGlcpNAcb1-4DGlcpNAcb1-OH
								String glycam_url = "http://glycam.org/url?condensed=" + Glycam;
								glycoseq_Glycam.addProperty(o_model.createProperty(
										PredicateTerms.has_link_to_glycam.getPredicate()), glycam_url);
							}

							o_model.write(pwGG, "TTL");
						} else {
							str_WURCS = "";
						}

					} catch (Exception e) {
						System.err.println("[error]\t" + line);
						e.printStackTrace();
					}
				}
			}
		}

		pwGG.close();

	}
}
