package main.java.org.glycoinfo.Glycans.RDF;

public enum PredicateTerms {

	// SIO
	has_SetItem         ("http://semanticscience.org/resource/SIO_000313"),  // has-setitem
	refers_to         ("http://semanticscience.org/resource/SIO_000628"),  // refers-to
	has_Value         ("http://semanticscience.org/resource/SIO_000300"),  // has_value
	has_attribute         ("http://semanticscience.org/resource/SIO_000008"),  // has-attribute

	// dcterms:references
	//dcterms: <http://purl.org/dc/terms/> .
	// <a resource> dcterms:references pubmed:24495517 .
	references        ("http://purl.org/dc/terms/references"),  // dcterms:references


	// Skos
	exactMatch         ("http://www.w3.org/2004/02/skos/core#exactMatch"),  // refers-to
	altLabel         ("http://www.w3.org/2004/02/skos/core#altLabel"),  // skos:altLabel
	note         ("http://www.w3.org/2004/02/skos/core#note"),  // skos:altLabel

	// DC
	title         ("http://purl.org/dc/elements/1.1/title"),
	license       ("http://purl.org/dc/elements/1.1/license"),

	// Foaf
	//primaryTopic
	primaryTopic        ("http://xmlns.com/foaf/0.1/primaryTopic"),

	

	// GlycoRDF
	has_Glycan         ("http://purl.jp/bio/12/glyco/glycan#has_glycan"),
	has_glycosequence  ("http://purl.jp/bio/12/glyco/glycan#has_glycosequence"),
	has_sequence       ("http://purl.jp/bio/12/glyco/glycan#has_sequence"),
	in_carbohydrate_format       ("http://purl.jp/bio/12/glyco/glycan#in_carbohydrate_format"),

	// GlyTouCan
	//glytoucan:has_primary_id
	//PREFIX glytoucan:  <http://www.glytoucan.org/glyco/owl/glytoucan#>
	has_primary_id        ("http://www.glytoucan.org/glyco/owl/glytoucan#has_primary_id"),

	// GlyCosmos
	has_link_to_glycam       ("http://glycosmos.org/owl#has_linkt_to_glycam"),



	NCIT_C25447         ("http://purl.obolibrary.org/obo/NCIT_C25447"),
	PROPERTYTYPE        ("http://www.w3.org/1999/02/22-rdf-syntax-ns#propertyType"),
	PROPERTYVALUE        ("http://www.w3.org/1999/02/22-rdf-syntax-ns#propertyValue");



	private String predicate;

	public String getPredicate() {
		return this.predicate;
	}

	PredicateTerms(String a_predicate){
		this.predicate = a_predicate;
	}

}
