package main.java.org.glycoinfo.Glycans.RDF;

public class Prefix {

	private String str_GGPrefix
				= "@prefix " + PrefixTerm.DCTERMS.getPrefix() + ": <" +  PrefixTerm.DCTERMS.getUri() + ">\n"
				+ "@prefix " + PrefixTerm.RDFS.getPrefix() + ": <" +  PrefixTerm.RDFS.getUri() + ">\n"
				+ "@prefix " + PrefixTerm.RDF.getPrefix() + ": <" +  PrefixTerm.RDF.getUri() + ">\n"
				+ "@prefix " + PrefixTerm.SIO.getPrefix() + ": <" +  PrefixTerm.SIO.getUri() + ">\n"
				+ "@prefix " + PrefixTerm.GLYCAN.getPrefix() + ": <" +  PrefixTerm.GLYCAN.getUri() + ">\n"
				+ "@prefix " + PrefixTerm.GTC.getPrefix() + ": <" +  PrefixTerm.GTC.getUri() + ">\n"
				+ "@prefix " + PrefixTerm.SKOS.getPrefix() + ": <" +  PrefixTerm.SKOS.getUri() + ">\n";


	public String getGGPrefix() {
		return this.str_GGPrefix;
	}
}
