package main.java.org.glycoinfo.Glycans.RDF;

public enum PrefixTerm {

	GCO         ("http://purl.jp/bio/12/glyco/conjugate#",                 "gco"),
	DCTERMS     ("http://purl.org/dc/terms/",                      "dcterms"),
	//DC         ("http://purl.org/dc/elements/1.1/",                "dc"),
	XSD         ("http://www.w3.org/2001/XMLSchema#",              "xsd"),
	RDFS        ("http://www.w3.org/2000/01/rdf-schema#",          "rdfs"),
	RDF         ("http://www.w3.org/1999/02/22-rdf-syntax-ns#",    "rdf"),
	FABIO       ("http://purl.org/spar/fabio/",                    "fabio"),
	UP          ("http://purl.uniprot.org/core/",                  "up"),
	// https://www.uniprot.org/uniprot/Q966M4
	UNIPROT     ("http://www.uniprot.org/uniprot/",                  "uniprot"),
	SIO         ("http://semanticscience.org/resource/",           "sio"),
	OBO         ("http://purl.obolibrary.org/obo/",                "obo"),
	//GG        ("http://purl.jp/bio/12/glyco/glycosmos/glycan/", "gg"),


	GLYCAN      ("http://purl.jp/bio/12/glyco/glycan#",            "glycan"),
	//GTC         ("http://glytoucan.org/Structures/Glycans/",       "gtc"),
	GTC         ("http://identifiers.org/glytoucan/", "gtc"),
	SKOS        ("http://www.w3.org/2004/02/skos/core#",           "skos"),
	COSMOS        ("http://glycosmos.org/owl#",           "cosmos"),
	GTCOWL      ("http://www.glytoucan.org/glyco/owl/glytoucan#",           "gtc"),
	//INFO        ("http://rdf.glycoinfo.org/glycan/",           "info");
	INFO        ("http://identifiers.org/glytoucan/", "info");
	//	https://identifiers.org/glytoucan:G00054MO

	// http://rdf.glycoinfo.org/glycan/G95631YS
	// PREFIX glytoucan:  <http://www.glytoucan.org/glyco/owl/glytoucan#>



	private String uri;
	private String prefix;

	public String getUri() {
		return this.uri;
	}

	public String getPrefix() {
		return this.prefix;
	}

	PrefixTerm(String a_uri, String a_prefix){
		this.uri = a_uri;
		this.prefix = a_prefix;
	}

}
