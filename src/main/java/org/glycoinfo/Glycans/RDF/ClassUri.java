package main.java.org.glycoinfo.Glycans.RDF;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;

public enum ClassUri {


		// GlycoRDF
		Saccharide     				("http://purl.jp/bio/12/glyco/glycan#Saccharide"),
		GlycoSequence     				("http://purl.jp/bio/12/glyco/glycan#Glycosequence"),

		carbohydrate_format_wurcs	("http://purl.jp/bio/12/glyco/glycan#carbohydrate_format_wurcs"),
		carbohydrate_format_iupac_extended	("http://purl.jp/bio/12/glyco/glycan#carbohydrate_format_iupac_extended"),
		carbohydrate_format_iupac_condensed	("http://purl.jp/bio/12/glyco/glycan#carbohydrate_format_iupac_condensed"),
		carbohydrate_format_iupac_short	("http://purl.jp/bio/12/glyco/glycan#carbohydrate_format_iupac_short"),
		carbohydrate_format_glycam_condensed	("http://purl.jp/bio/12/glyco/glycan#carbohydrate_format_glycam_condensed"),
		carbohydrate_format_glycoct	("http://purl.jp/bio/12/glyco/glycan#carbohydrate_format_glycoct"),

		// SIO
		Set         ("http://semanticscience.org/resource/SIO_000289"), // sio:set
		SetItem         ("http://semanticscience.org/resource/SIO_001258"), // sio:setitem
		Disease      ("http://semanticscience.org/resource/SIO_010299"),

		Taxon							("http://purl.uniprot.org/core/Taxon"),
		Citation      	("http://purl.jp/bio/12/glyco/glycan#Citation");


		private String classUri;

		public Resource getClassUri() {
			Model o_model = ModelFactory.createDefaultModel();
			Resource resource = o_model.createResource(this.classUri);

			return resource;
		}

		ClassUri(String a_class){
			this.classUri = a_class;
		}

}
