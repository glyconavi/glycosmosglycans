# glycosmosglycans

## Requirements

* Software development language: Java 1.8 or more
* Build tool: maven 3.5 or higher


## build

```
$ mvn clean compile assembly:single
```

## Example

* input data tab separated file (tsv)

```
$ java -jar target/glycosmosglycans.jar -f <tsv file>
```

## RDF check

```
$ rapper -i turtle -o ntriples 2021-11-12-17-52-GlyTouCan-WURCS.tsv_2021-11-12_Glycans-0.4.0.ttl > 2021-11-12-17-52-GlyTouCan-WURCS.tsv_2021-11-12_Glycans-0.4.0.ttl.n3
rapper: Parsing URI file:///Users/yamada/git/gitlab/glyconavi/glycosmosglycans/glycosmosglycans/2021-11-12-17-52-GlyTouCan-WURCS.tsv_2021-11-12_Glycans-0.4.0.ttl with parser turtle
rapper: Serializing with serializer ntriples
rapper: Parsing returned 2233076 triples
```

## compress

```
gzip 2021-11-12-17-52-GlyTouCan-WURCS.tsv_2021-11-12_Glycans-0.4.0.ttl

ls -l
2021-11-12-17-52-GlyTouCan-WURCS.tsv_2021-11-12_Glycans-0.4.0.ttl.gz
```


## Load triplestore

* graph: http://glyconavi.org/glytoucan-seq

* Delete 

```
log_enable(2,1);
SPARQL CLEAR GRAPH <http://glyconavi.org/glytoucan-seq>;
```

* load

```
DELETE FROM DB.DBA.LOAD_LIST WHERE ll_graph = 'http://glyconavi.org/glytoucan-seq';
DELETE FROM DB.DBA.LOAD_LIST;

ld_dir ('/virtuoso/glytoucan', '2021-11-12-17-52-GlyTouCan-WURCS.tsv_2021-11-12_Glycans-0.4.0.ttl.gz', 'http://glyconavi.org/glytoucan-seq');

rdf_loader_run();
VT_INC_INDEX_DB_DBA_RDF_OBJ();
select * from DB.DBA.LOAD_LIST ORDER BY ll_started DESC;

```




## release note

2021-11-12   version 0.4.0

* update glycanformatconverter 2.5.2 to 2.7.0

* Glycan URI -> "http://identifiers.org/glytoucan/"

* add licence: CC-BY 4.0



